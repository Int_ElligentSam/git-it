#!/usr/bin/python3


def retrieve_number(number: str) -> str:
    """
    A simple function for retrieving/rearranging phone number 
    from a dissorganised sequences of eleven numbers
    """
    # digit sequences should match phone limit
    if len(number) == 11:
        # sort as iterables e.g List [0, 0, 1, 1, 2, 4, 4, 4, 5, 6, 8]
        seq = sorted([int(n) for n in number])
        # step 1
        seq[1], seq[len(seq)-1] = seq[len(seq)-1],  seq[1]
        seq[2], seq[len(seq)-1] = seq[len(seq)-1],  seq[2]
        
        # step 2
        temp = seq[3:5]
        seq[3:6] = reversed([x for x in seq[2:] if x > seq[3]][3:])
        
        # step 3
        temp2 = seq[6:8]
        seq[6:8] = temp

        # step 4
        seq[8:10] = temp2

        return seq

    # numbers don't lie     
    return None
if __name__ == "__main__":
    print(f'{retrieve_number("64401128450")}')